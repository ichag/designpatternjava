package FactorySchokoladen;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by max on 09.01.14.
 */
public class Schokoladenfabrik {

    private int monat;
    private GregorianCalendar datum;
    private SchokoInterface instanceOfSchokolade;

    public Schokoladenfabrik() {
        datum = new GregorianCalendar();
        monat = datum.get(Calendar.MONTH)+1;
        instanceOfSchokolade = null;
    }


    public String getSchokoladePerQuatal() {
        switch (monat) {
            case 7:case 8:case 9:
                instanceOfSchokolade = new Weihnachtsmann();
                break;
            case 1:case 2:case 3:
                instanceOfSchokolade = new Osterhase();
                break;
            default:
                instanceOfSchokolade = new Schokolade();
                break;
        }
        return instanceOfSchokolade.fabriziere();
    }
}
