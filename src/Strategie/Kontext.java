package Strategie;
public class Kontext {
	private int dauer;
	private double preis;
	private double rabatt;
	
	public Kontext(int dauer) {
		this.dauer = dauer;
		this.preis = 19;
	}

	public void setRabatt() {
		if (this.dauer < 10) {
			this.rabatt = new RabattNullProzent().berechneRabatt();
		} else {
			if (this.dauer >= 10 && this.dauer <= 20) {
				this.rabatt = new RabattZehnProzent().berechneRabatt();
			} else {
				if (this.dauer > 20) {
					this.rabatt = new RabattZwanzigProzent().berechneRabatt();
				}
			}
		}
	}
	public double berechnePreis() {
		preis = ((100 * preis) / this.rabatt)*dauer;
		return preis;
	}
	

}
