package MVCTableDAO.dao;

import MVCTableDAO.model.*;

import java.util.List;

public interface MitarbeiterDAOInterface {
    public void createMitarbeiter(Employee ma) throws Exception;

    public Employee readMitarbeiter(String id) throws Exception;

    public List<Employee> readAllMitarbeiter() throws Exception;

    public void updateMitarbeiter(Employee ma) throws Exception;

    public void deleteMitarbeiter(Employee ma) throws Exception;
}