package MVCTableDAO.dao;

import MVCTableDAO.model.Employee;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MitarbeiterDAOToObjectStream implements MitarbeiterDAOInterface {
    private File file = new File("ma.obj");
    private Map<String, Employee> mitarbeiterHashMap = new HashMap<String, Employee>();

    @SuppressWarnings(value = "unchecked")
    private void readData() throws Exception {
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream objIn = new ObjectInputStream(fis);
        mitarbeiterHashMap = (HashMap<String, Employee>) objIn.readObject();
    }

    @SuppressWarnings(value = "unchecked")
    private void writeData() throws Exception {

        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream objOut = new ObjectOutputStream(fos);
        objOut.writeObject(mitarbeiterHashMap);
    }

    @Override
    public void createMitarbeiter(Employee ma) throws Exception {
        if (mitarbeiterHashMap.containsKey(ma.getId())) {
            throw new Exception("Doppelter Schluessel...");

        } // end of if
        mitarbeiterHashMap.put(ma.getId(), ma);
        writeData();
    }

    public Employee readMitarbeiter(String id) throws Exception {
        readData();
        return mitarbeiterHashMap.get(id);
    }

    public List<Employee> readAllMitarbeiter() throws Exception {
        readData();
        List<Employee> allEmployee = new ArrayList<Employee>(mitarbeiterHashMap.values());
        return allEmployee;
    }

    public void updateMitarbeiter(Employee ma) throws Exception {
        readData();
        mitarbeiterHashMap.remove(ma);
        mitarbeiterHashMap.put(ma.getId(), ma);
        writeData();
    }

    public void deleteMitarbeiter(Employee ma) throws Exception {
        readData();
        mitarbeiterHashMap.remove(ma.getId());
        writeData();
    }
}