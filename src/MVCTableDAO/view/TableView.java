
package MVCTableDAO.view;


import MVCTableDAO.model.Department;
import MVCTableDAO.model.Employee;
import MVCTableDAO.model.TableDAO;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.Files;
import java.io.File;


public class TableView extends JFrame implements WindowListener {

  private TableDAO model;
  private JTextField txtId, txtFirstName, txtLastName, txtDepartmentId, txtDepartmentDescription;
  private JPopupMenu rightClickMouseMenu;
  private JMenuItem rightClickLoadMenuItem;
  private ImageIcon imageIcon;
  private JLabel imageLabel;
  private JTable mainTable;
  private Path path;
  private Employee currentEmployee = new Employee("", "" , "", null);

    public void onRightClick(MouseEvent e) {
      if (e.isPopupTrigger()) {
        rightClickMouseMenu.show(e.getComponent(), e.getX(), e.getY());
      }
    }
    public void onLoadImage() {
      FileSystem fs = FileSystems.getDefault();
      path = fs.getPath("/home/max/IdeaProjects/designpatternjava/src/MVCTableDAO/images/");
      JFileChooser chooser = new JFileChooser("/home/max/IdeaProjects/designpatternjava/src/MVCTableDAO/images/");
      int returnValue = chooser.showOpenDialog(null);
      byte[] data;
      if(returnValue == JFileChooser.APPROVE_OPTION) {
        try {
          String pathToFile = chooser.getSelectedFile().getAbsolutePath();
          data = getByteArrayFromFile(pathToFile);
          if (data != null) {
            System.out.println("File gelesen!");
          }else {
            System.err.println("File lesen fehlgeschlagen!");
            System.out.println(pathToFile);
          }
          imageIcon = new ImageIcon(data);
          Employee temp = model.readMitarbeiter(txtId.getText());
          temp.setImage(data);
          model.insertImageForEmployee(temp);
          imageLabel.setIcon(imageIcon);
        } catch (Exception e) {
           e.printStackTrace();
        }
      }
    }

  public byte[] getByteArrayFromFile(String filePath){
    byte[] result=null;
    FileInputStream fileInStr=null;
    try{
      File imgFile=new File(filePath);
      fileInStr=new FileInputStream(imgFile);
      long imageSize=imgFile.length();

      if(imageSize>Integer.MAX_VALUE){
        return null;
      }

      if(imageSize>0){
        result=new byte[(int)imageSize];
        fileInStr.read(result);
      }
    }catch(Exception e){
      e.printStackTrace();
    } finally {
      try {
        fileInStr.close();
      } catch (Exception e) {
      }
    }
    return result;
  }

    public TableView() {
        model = new TableDAO();
        this.addWindowListener(this);
        this.setMinimumSize(new Dimension(768, 600));
        this.setLayout(null);

        mainTable = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(mainTable);
        mainTable.setAutoCreateRowSorter(true);
        mainTable.getSelectionModel().setSelectionMode(0);
        mainTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                mainTable_SelectionChanged(e);


            }
        });
        scrollPane.setBounds(0, 0, getWidth() - 15, 200);
        this.add(scrollPane);

        JLabel lblId = new JLabel("ID:");
        lblId.setBounds(5, 205, 70, 20);
        this.add(lblId);

        txtId = new JTextField();
        txtId.setText("");
        txtId.setBounds(75, 205, 100, 20);
        this.add(txtId);

        JLabel lblFirstName = new JLabel("Vorname:");
        lblFirstName.setBounds(5, 230, 70, 20);
        this.add(lblFirstName);

        txtFirstName = new JTextField();
        txtFirstName.setBounds(75, 230, 100, 20);
        txtFirstName.setText("");
        this.add(txtFirstName);

        JLabel lblLastName = new JLabel("Nachname:");
        lblLastName.setBounds(5, 255, 70, 20);
        this.add(lblLastName);

        txtLastName = new JTextField();
        txtLastName.setBounds(75, 255, 100, 20);
        txtLastName.setText("");
        this.add(txtLastName);

      /*
      START Department
      ____________________________________________________________
       */

        JLabel lblkuerzel = new JLabel("Abt ID:");
        lblkuerzel.setBounds(5, 280, 100, 20);
        this.add(lblkuerzel);

        txtDepartmentId = new JTextField();
        txtDepartmentId.setBounds(75, 280, 100, 20);
        txtLastName.setText("");
        this.add(txtDepartmentId);


        JLabel lblDescription = new JLabel("Abt Bez:");
        lblDescription.setBounds(5, 305, 100, 20);
        this.add(lblDescription);



      txtDepartmentDescription = new JTextField();
      txtDepartmentDescription.setBounds(75, 305, 100, 20);
      txtDepartmentDescription.setText("");
      this.add(txtDepartmentDescription);

       /*
      END Department
      _____________________________________________________________
       */

      /*
      Start Image
       */
      imageIcon = new ImageIcon("/home/max/IdeaProjects/designpatternjava/src/MVCTableDAO/images/dummy.jpg");
      imageLabel = new JLabel(imageIcon);
      imageLabel.setBounds(185, 305, 142, 196);
      this.add(imageLabel);
      /*
      End Image
      _____________________________________________________________
       */
      /*
      Start right Mousekey listner
       */

      rightClickMouseMenu = new JPopupMenu();
      rightClickLoadMenuItem = new JMenuItem("Foto laden");
      rightClickMouseMenu.add(rightClickLoadMenuItem);
      imageLabel.addMouseListener(new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent e) {
          onRightClick(e);
        }
      });
      rightClickLoadMenuItem.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          onLoadImage();
        }
      });

      /*
      END right mousekey listner
       */


        JButton btnCreate = new JButton("Neu");
        btnCreate.setBounds(185, 205, 100, 20);
        btnCreate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCreate_ActionPerformed(e);
            }
        });
        this.add(btnCreate);

        JButton btnSearch = new JButton("Suchen");
        btnSearch.setBounds(185, 230, 100, 20);
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnSearch_ActionPerformed(e);
            }
        });
        this.add(btnSearch);

        JButton btnUpdate = new JButton("Update");
        btnUpdate.setBounds(185, 255, 100, 20);
        btnUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnUpdate_ActionPerformed(e);
            }
        });
        this.add(btnUpdate);

        JButton btnDelete = new JButton("Löschen");
        btnDelete.setBounds(185, 280, 100, 20);
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnDelete_ActionPerformed(e);
            }
        });
        this.add(btnDelete);

        pack();
    }


    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                TableView ex = new TableView();
                ex.setVisible(true);
            }
        });
    }

    public void btnCreate_ActionPerformed(ActionEvent e) {
      if(!(txtDepartmentId.getText().isEmpty() && txtDepartmentDescription.getText().isEmpty())) {
        Department dep = new Department(txtDepartmentId.getText(), txtDepartmentDescription.getText());
        try {
          model.createDepartment(dep);
        } catch (Exception e1) {
          e1.printStackTrace();
        }
      }
      if (!(txtId.getText().isEmpty() || txtFirstName.getText().isEmpty() || txtLastName.getText().isEmpty())) {
          try {
              Department dep = new Department(txtDepartmentId.getText(), txtDepartmentDescription.getText());
              model.createMitarbeiter(new Employee(txtId.getText(), txtFirstName.getText(), txtLastName.getText(), dep));
          } catch (Exception ex) {
              ex.printStackTrace();
              JOptionPane.showMessageDialog(null, "Entweder wurde keine ID eingegen oder die ID ist bereits vorhanden!", "Fehler", JOptionPane.ERROR_MESSAGE);
          }

          mainTable.updateUI();
      } else {
          JOptionPane.showMessageDialog(null, "Bitte ID, Vorname und Nachname angeben!", "Fehler", JOptionPane.ERROR_MESSAGE);
      }

    }

    public void btnSearch_ActionPerformed(ActionEvent e) {
        try {
            Employee temp = model.readMitarbeiter(txtId.getText());
            txtFirstName.setText(temp.getVorname());
            txtLastName.setText(temp.getNachname());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        mainTable.updateUI();
    }

    public void btnUpdate_ActionPerformed(ActionEvent e) {
        try {
          Department dep = new Department(txtDepartmentId.getText(), txtDepartmentDescription.getText());
          model.updateMitarbeiter(new Employee(txtId.getText(), txtFirstName.getText(), txtLastName.getText(), dep));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        mainTable.updateUI();
    }

    public void btnDelete_ActionPerformed(ActionEvent e) {
        try {
          Department dep = new Department(txtDepartmentId.getText(), txtDepartmentDescription.getText());
          model.deleteMitarbeiter(new Employee(txtId.getText(), txtFirstName.getText(), txtLastName.getText(), dep));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        mainTable.updateUI();
    }

    public void mainTable_SelectionChanged(ListSelectionEvent e) {
        if (mainTable.getSelectedRow() < 0 || mainTable.getSelectedRow() >= mainTable.getRowCount())
            return;
        txtId.setText(mainTable.getValueAt(mainTable.getSelectedRow(), 0).toString());
        txtFirstName.setText(mainTable.getValueAt(mainTable.getSelectedRow(), 1).toString());
        txtLastName.setText(mainTable.getValueAt(mainTable.getSelectedRow(), 2).toString());
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        model.close();
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
