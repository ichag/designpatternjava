package MVCTableDAO.model;

public class Employee implements java.io.Serializable {
    private String id, vorname, nachname;
    private Department department;
    private int columns;
    private byte[] image;

    public Employee(String id, String vorname, String nachname, Department pDepartment) {
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.department = pDepartment;
        this.columns = 4;
    }

  public byte[] getImage() {
    return image;
  }

  public void setImage(byte[] image) {
    this.image = image.clone();
  }

  public String getId() {
        return this.id;
    }

  public void setId(String id) {
    this.id = id;
  }

  public void setVorname(String vorname) {
    this.vorname = vorname;
  }

  public void setNachname(String nachname) {
    this.nachname = nachname;
  }

  public String getVorname() {
        return this.vorname;
    }

    public int getFieldsCount() {
        return this.columns;
    }

    public String getNachname() {
        return this.nachname;
    }

    public String toString() {
        return id + " " + vorname + " " + nachname + " " + getDepartment().getDescription();
    }

  public Department getDepartment() {
    return department;
  }

  public void setDepartment(Department department) {
    this.department = department;
  }
}