package MVCTableDAO.model;

import javax.swing.table.AbstractTableModel;
import java.util.List;
import MVCTableDAO.dao.*;
import MVCTableDAO.model.*;

/**
 * Created by max on 09.12.13.
 */
public class TableDAO extends AbstractTableModel {
    private List<Employee> liste;
    private MitarbeiterDAOToSQLite dao;
    Employee myEmployee;

    public TableDAO() {
        this.dao = new MitarbeiterDAOToSQLite();
        updateList();
    }

    @Override
    public int getRowCount() {
        try {
            return dao.readAllMitarbeiter().size();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Id";
            case 1:
                return "Vorname";
            case 2:
                return "Nachname";
            case 3:
                return "Abteilung";
            default:
                return null;
        }
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

public void updateList() {
        try {
            liste = dao.readAllMitarbeiter();
        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
    }


    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (liste.get(rowIndex) != null) {
            myEmployee = liste.get(rowIndex);
        } else {
            return null;
        }
        updateList();
        switch (columnIndex) {
            case 0:
                return liste.get(rowIndex).getId();
            case 1:
                return liste.get(rowIndex).getVorname();
            case 2:
                return liste.get(rowIndex).getNachname();
            case 3:
                return liste.get(rowIndex).getDepartment().getDescription();

            default:
                return null;
        }
    }

    public void createMitarbeiter(Employee employee) throws Exception {
        dao.createMitarbeiter(employee);
        updateList();
        fireTableDataChanged();
    }

    public Employee readMitarbeiter(String id) throws Exception {
        return dao.readMitarbeiter(id);
    }

    public List<Employee> readMitarbeiters() throws Exception {
        return dao.readAllMitarbeiter();
    }

    public void updateMitarbeiter(Employee employee) throws Exception {
        dao.updateMitarbeiter(employee);
        updateList();
        fireTableDataChanged();
    }

    public void deleteMitarbeiter(Employee employee) throws Exception {
        dao.deleteMitarbeiter(employee);
        updateList();
        fireTableDataChanged();
    }

  public void insertImageForEmployee(Employee employee) throws Exception {
    dao.updateImageForEmployee(employee);
    updateList();
    fireTableDataChanged();
  }

  public void createDepartment(Department department) throws Exception {
    dao.createDepartment(department);
    updateList();
    fireTableDataChanged();
  }

    public void close() {
        dao.closeDB();
    }
}
