package Observer;
import java.util.Observable;

public class Kasse extends Observable {

	private Double betrag;

	public Kasse() {
		super();
		this.betrag = 0.0;
	}

	public Double getBetrag() {
		return betrag;
	}

	public void increaseBetrag(Double betrag) {
		this.betrag += betrag;
		super.setChanged();
		super.notifyObservers(betrag);
	}

	public void decreaseBetrag(Double betrag) {
		this.betrag -= betrag;
		super.setChanged();
		super.notifyObservers(betrag);
	}

	public String toString() {
		return ("Der Betrag der Kasse ist aktuell " + this.getBetrag());
	}
}