package Observer;
public class Main {

	public static void main(String ... args) {

		/**
		 * Initialize Observer
		 */
		SingletonBeobachter kObs1 = SingletonBeobachter.BEOBACHTER;

		/**
		 * Create a Kasse
		 */
		Kasse k1 = new Kasse();

		/**
		 * Iterate over Kassen Observer Objects
		 */
			k1.addObserver(kObs1);

		/**
		 * Manipulate Kasse
		 */
		System.out.println(k1.toString());
		k1.increaseBetrag(15.0);
		k1.decreaseBetrag(3.0);
		System.out.println(k1.toString());
	}
}