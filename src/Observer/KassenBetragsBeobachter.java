package Observer;
import java.util.Observable;
import java.util.Observer;


public class KassenBetragsBeobachter implements Observer {
	
	@Override
	public void update(Observable o, Object betrag) {
		System.out.println("Der Betrag der Kasse wurde geändert um: " + betrag + " €");
	}
}