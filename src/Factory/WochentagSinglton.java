package Factory;

import java.util.Calendar;

/**
 * Created by max on 05.12.13.
 */
public enum WochentagSinglton {

    WOCHENTAG_SINGLTON;

    public String getDayOfWeek() {

        switch (Calendar.DAY_OF_WEEK) {
            case 0:
                return "Montag";
            case 1:

                return "Dienstag";
            case 2:

                return "Mittwoch";
            case 3:

                return "Donnerstag";
            case 4:

                return "Freitag";
            case 5:

                return "Samstag";
            case 6:

                return "Sonntag";
            default:
                break;
        }
        return null;
    }

    }
