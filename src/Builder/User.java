package Builder;

public class User { 
	  private final String firstName;
	  private final String lastName;
	  private int age;
	  private String phone;
	  private String adress;
	  
	  public String getFirstName() {
	    return this.firstName;
	  }
	  public String getLastName() {
	    return this.lastName;
	  }
	  public int getAge() {
	    return this.age;
	  }
	  public String getPhone() {
	    return this.phone;
	  }
	  public String getAdress() {
	    return this.phone;
	  }
	  
	  public void setAge(int age) {
	    this.age = age;
	  }
	  
	  public void setPhone(String phone) {
	    this.phone = phone;
	  }
	  public void setAdress(String adress) {
	    this.phone = adress;
	  }
	  private User(String firstName, String lastName) {
	    this.firstName = firstName;
	    this.lastName = lastName;
	  }
	  @Override
	  public String toString() {
	    return "Vorname: " + this.firstName + " Nachname: " + this.lastName + " Alter: " + this.age + " Telephone: " +this.phone;
	  } 
	  public static class Builder {
	    
		  private User user;
	    
	    public Builder(String firstName, String lastName) {
	    	user = new User(firstName,lastName);
	    }
	    public Builder withAge(int age) {
	      user.setAge(age);
	      return this;
	    }
	    public Builder withPhone(String phone) {
	      user.setPhone(phone);
	      return this;
	    }
	    public Builder withAdress(String adress) {
	      user.setAdress(adress);
	      return this;
	    }
	    public User build() {
	      return user;
	    }
		
	  }
	}