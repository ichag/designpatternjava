package Builder;

public class Anwendung {
	  public static void main(String ... args) {
	    User myBuild = new User.Builder("Max", "Hellwig").withAge(18).build();
	    System.out.println(myBuild);
	  }
	  
	}
