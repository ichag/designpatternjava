package MVCJava.Mitarbeiter;

import java.util.List;

/**
 * Created by max on 09.12.13.
 */
public class MitarbeiterViewController {

    private MitarbeiterModel model;

    public MitarbeiterViewController() throws Exception {
        model = new MitarbeiterModel(this);
        processUserAction();
    }

    public void processUserAction() {
        int i = this.userAction();
        model.loesche(i);
    }

    public void updateView(List<Mitarbeiter> liste) {
        for (Mitarbeiter ma : liste) {
            System.out.println(ma);
        }
        System.out.println();
    }

    public int userAction() {
        int i = (int) (Math.random() * 3);
        return i;
    }

}
