package MVCJava.Mitarbeiter;

public class Mitarbeiter implements java.io.Serializable {
    private String id, vorname, name;

    public Mitarbeiter(String id, String vorname, String name) {
        this.id = id;
        this.vorname = vorname;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getVorname() {
        return vorname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    @Override
    public String toString() {
        return id + ": " + vorname + " " + name;
    }
}