package MVCJava.Mitarbeiter;
import java.util.*;
public class MitarbeiterModel {
  private Map<String, Mitarbeiter> mitarbeiter = new HashMap<String, Mitarbeiter>();
  private MitarbeiterViewController view;
  public MitarbeiterModel(MitarbeiterViewController view) {
    this.view = view;
    Mitarbeiter m1 = new Mitarbeiter("MA-4711", "Frank N.", "Stein");
    Mitarbeiter m2 = new Mitarbeiter("MA-4712", "Susi", "Sorglos");
    Mitarbeiter m3 = new Mitarbeiter("MA-4713", "Eberhard", "Bolte");
    mitarbeiter.put(m1.getId(),m1);
    mitarbeiter.put(m2.getId(),m2);
    mitarbeiter.put(m3.getId(),m3);   
    view.updateView(new ArrayList<Mitarbeiter> (mitarbeiter.values()));
  }
  public void loesche (int i) {
    ArrayList<Mitarbeiter> liste = new ArrayList<Mitarbeiter>(mitarbeiter.values());  
    Mitarbeiter mit = liste.get(i);
    mitarbeiter.remove(mit.getId());
    view.updateView(new ArrayList<Mitarbeiter> (mitarbeiter.values())); 
  }
}
