package Composite;
import java.util.*;
/**
 * Created by max on 08.01.14.
 */
public class Node extends Mitarbeiter {

    private List<Mitarbeiter> liste;

    public Node(String name, String position, double gehalt) {
        super(name, position, gehalt);
        liste = new ArrayList<Mitarbeiter>();
    }

    public String ausgeben() {
        String output = new String();
        output += super.ausgeben() + "\n";
        for(Mitarbeiter ma: liste) {
            output += ma.ausgeben() + "\n";
        }
        return output;
    }
    public void addMitarbeiter(Mitarbeiter ma) {
        liste.add(ma);
    }

}
