package Composite;

public abstract class Mitarbeiter {
	private String name;
	private String funktion;
	private double gehalt;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFunktion() {
		return funktion;
	}

	public void setFunktion(String funktion) {
		this.funktion = funktion;
	}

	public double getGehalt() {
		return gehalt;
	}

	public Mitarbeiter(String name, String funktion, double gehalt) {
		this.name = name;
		this.funktion = funktion;
		this.gehalt = gehalt;
	}
	public String ausgeben() {
		return this.name + ", " + this.funktion + ", "+this.gehalt;
	}

}
