package Composite;

/**
 * Created by max on 08.01.14.
 */
public class Client {

    public static void main(String ... args) {
        Node ma1 = new Node("1max", "chef", 9999.0);
        Mitarbeiter ma2 = new Leaf("2max", "das", 9999.0);

        Mitarbeiter ma3 = new Leaf("3max", "gsdf", 9999.0);
        Mitarbeiter ma4 = new Leaf("4max", "vdfbvr", 9999.0);

        Node ma5 = new Node("5max", "vsdfds", 9999.0);
        Mitarbeiter ma6 = new Node("6max", "odgh", 9999.0);

        ma1.addMitarbeiter(ma2);
        ma1.addMitarbeiter(ma3);
        ma1.addMitarbeiter(ma4);
        ma1.addMitarbeiter(ma5);
        ma5.addMitarbeiter(ma6);



        System.out.println(ma1.ausgeben());
    }
}
